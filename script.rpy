﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define r = Character("Rosemary", color="#ed6480")
define g = Character("Garlic")
define nn = nvl_narrator #https://www.renpy.org/doc/html/nvl_mode.html

define gui.nvl_height = 1038 #default 173
define gui.nvl_text_width = 700 #default 740
define gui.nvl_thought_width = 1100 #default 740
define gui.nvl_thought_xalign = 0.5 #this and the one below are for centering the text don't ask me how it works
define gui.nvl_thought_xpos = 0.5

# The game starts here.

label start:

    
    #scene bg bliss
    scene bg black


    #narrator "b\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\nb\n"
    nn """
    \n\n
    I swear by the power of the Wind, Trees, Sea, and Stone, to protect and keep the land, to forever hold it dear.
    \n\n
    I pledge myself to uphold the balance, keep history alive, and to never be constrained by its relics.
    \n\n
    I vow to stay curious and engaged, to look at the world with a questioning eye and ask ‘why?’.
    \n\n
    I shall use my ability to protect what is precious, even if it brings about my own end. To honour all that came before me and those who may yet come after.
    \n\n
    I will add my name to the legacy of courage that the title of Guardian imparts.
    \n\n
    I honour my name, my family, and my one true self, my multitudes.
    \n\n
    If I break this oath, if my cowardice breathes sloth or wilful ignorance, may my body be cast upon the stones, my sword broken, and my name erased.
    \n\n
    This, I vow.
    """
    nvl clear #I thought "nvl clear" was necessary every time you wanted a blank slate but now I'm not sure. Will have to check.
    nn "\n\n…"
    nvl clear
    nn """
    \n\n
    Me, a guardian, can you believe it? I never wanted this. I never asked for this.
    \n\n
    I was content back home, working in my family’s alchemy shop. Sure, I was isolated and a shut in, known around town as that one creepy kid, Garlic, the one who muttered to himself and did nothing but scowl. 
    \n\n
    I was unsociable, antagonistic, and friendless, yes, but content.
    \n\n
    Unfortunately for me however, I was also talented.
    \n\n
    Old magic seemed to flow through my veins and I could cast with only a little effort. I was a ‘dab hand at spell craft’ as my Da used to say.
    """
    nvl clear

    nn """
    \n\n
    I admit, I may have had some…small affinity for it. Nothing to write home about though. Just slightly stronger than all of the wise women and hedge wizards in my quaint little village of Bogheap.
    \n\n
    Now I know what you’re thinking. ‘So what Garlic, you have a knack for magic whoop-di-do. Guardians require more than just some fancy challenging or spell flinging.’ And you know what? You would be right.
    \n\n
    But there was extra hitch…
    \n\n
    …
    \n\n
    Guardianship seemed to run in the family.
    """
    nvl clear
    nn """
    ..
    \n\n
    Well, sort of.
    \n\n
    I have this aunt. Aunt Beetroot.
    \n\n
    She’s the star of our family line. A powerful spellcaster who just so happened to save some rich family passing through the area one day when she was around my age. They were so grateful they actually pulled some strings and got her accepted into The High Guardian Academy. An act of ‘good will’ they called it.
    \n\n
    To make a long story short, she passed her trials. Beetroot Chives from the little town of Bogheap actually managed to become a Guardian. It was like something out of a fairy tale. She became a valiant hero who would come visit us on holidays and regale us with stories of her heroics. 
    \n\n
    I had to admit when I was knee high, I’d sit there on the floor by the fire with stars in my eyes, dreaming of becoming a Guardian one day like every other kid. But then of course, I grew older and wiser. I accepted my lot in life. I was never going to leave Bogheap and that was fine by me.
    """
    nvl clear

    nn """
    \n\n
    Not like Aunt Beetroot. She hit the jackpot and had it all made for her.
    \n\n
    And you know what?
    \n\n
    Good for her.
    \n\n
    She got lucky and managed to get out. I honestly can’t be jealous about that. In life there are winners and losers. You’ll never get a head in life without some else willing to give you that leg up. It’s just how life is.
    \n\n
    And to my dismay, it would seem all these years later, she would pay this ‘good will’ forward.
    \n\n
    As it turns out unbeknownst to me and only me (because apparently everyone else in the family knew. Thanks Ma.) She was keeping an eye on me. Watching me ‘grow into a bright young man bursting with potential.’
    \n\n
    And then one day she came with a letter.
    """

    show bg letter with dissolve #https://www.renpy.org/doc/html/transitions.html
    

    "THE letter.\nA letter of acceptance…To High Guardian Academy."

    #window hide dissolve
    window hide
    show bg acceptance with dissolve #https://www.renpy.org/doc/html/transitions.html
    pause
    #window show dissolve
    window show

    nn """
    \n\n
    Just like how she got a leg up in this world, so too have I.
    \n\n
    …
    \n\n
    …
    \n\n
    …
    \n\n
    Fuck me.
    """

    play music "Eastenders - 2009 - Ending.mp3" noloop

    pause

    show rosemary smile placeholder



    r "Welcome ta Cornwood!"

    r "testestsetetsetestsetsetsetsetstetes!"

    # This ends the game.

    return
